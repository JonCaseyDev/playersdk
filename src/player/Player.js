"use strict";
exports.__esModule = true;
var Player = /** @class */ (function () {
    function Player(playerService) {
        var _this = this;
        this.playerService = playerService;
        this.video = {};
        this.state = {
            playing: false,
            paused: false,
            loaded: false,
            ready: false
        };
        this.isInit = false;
        this.state.playing = false;
        this.playerService.fetchManifest().then(function (response) {
            _this.mpdResponse = response;
            _this.init(response);
            _this.isInit = true;
        });
    }
    Player.prototype.init = function (mpdRes) {
        this.video.videoAdaptation = this.getVideoAdaptation(mpdRes);
        this.video.audioAdaptation = this.getAudioAdaptation(mpdRes);
        // loads video source
        this.load();
        this.video.audioSource = this.loadAudioSource();
        this.video.playbackDuration = this.getDuration(this.video.videoAdaptation);
        this.video.chosenRepresentation = this.getBestRepresentation(this.video);
        var videoCodec = this.video.videoAdaptation.$.codecs;
        var audioCodec = this.video.audioAdaptation.$.codecs;
        this.video.codecs = [];
        this.video.codecs.push(videoCodec, audioCodec);
    };
    Player.prototype.getBestRepresentation = function (video) {
        /**
         * note: this should be calculated by the clients bandwidth.
         * Assuming here that 1080p is the best representation.
         */
        var r;
        video.videoAdaptation.Representation.forEach(function (rep) {
            if (rep.$.height === "1080") {
                r = rep;
            }
        });
        return r;
    };
    Player.prototype.getVideoAdaptation = function (mpdRes) {
        var adaptation;
        mpdRes.Period[0].AdaptationSet.forEach(function (set) {
            if (set.$.mimeType.includes("video")) {
                adaptation = set;
            }
        });
        return adaptation;
    };
    Player.prototype.getAudioAdaptation = function (mpdRes) {
        var adaptation;
        mpdRes.Period[0].AdaptationSet.forEach(function (set) {
            if (set.$.mimeType.includes("audio")) {
                adaptation = set;
            }
        });
        return adaptation;
    };
    Player.prototype.getDuration = function (videoAdaptation) {
        var duration = parseInt(videoAdaptation.SegmentTemplate[0].$.duration);
        var timescale = parseInt(videoAdaptation.SegmentTemplate[0].$.timescale);
        var minutes = Math.floor(duration / timescale);
        return Math.floor(minutes * 60);
    };
    Player.prototype.loadAudioSource = function () {
        var mediaSource;
        this.mpdResponse.Period[0].AdaptationSet.forEach(function (set) {
            if (set.$.mimeType.includes("audio")) {
                mediaSource = set.SegmentTemplate[0].$.media;
            }
        });
        return mediaSource;
    };
    // break into rest api & stub responses
    Player.prototype.fetch = function () { };
    Player.prototype.stream = function () { };
    Player.prototype.play = function () {
        this.state.playing = true;
    };
    Player.prototype.pause = function () {
        if (this.state.playing) {
            this.state.playing = false;
            this.state.paused = true;
            return;
        }
    };
    Player.prototype.stop = function () {
        this.state.playing = false;
    };
    Player.prototype.load = function () {
        var _this = this;
        this.mpdResponse.Period[0].AdaptationSet.forEach(function (set) {
            if (set.$.mimeType.includes("video")) {
                _this.video.videoSource = set.SegmentTemplate[0].$.media;
            }
        });
    };
    Player.prototype.unload = function () {
        this.video = {};
    };
    // register and unregister event listeners.
    Player.prototype.on = function () { };
    Player.prototype.off = function () { };
    Player.prototype.isPlaying = function () {
        return this.state.playing;
    };
    Player.prototype.isPaused = function () {
        return this.state.paused;
    };
    Player.prototype.isReady = function () {
        if (this.isInit) {
            return true;
        }
        return false;
    };
    Player.prototype.isLoaded = function () {
        return this.video.videoSource !== null;
    };
    // used by player controls or by player to improve video stream
    Player.prototype.changeRepresentation = function (type) { };
    Player.prototype.destroy = function () {
        this.video = {};
    };
    Player.prototype.getCurrentTime = function () {
        return this.video.playbackDuration;
    };
    return Player;
}());
exports.Player = Player;
exports["default"] = Player;
