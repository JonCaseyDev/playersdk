import { Video, PlayerInterface } from "./../interface/player.interface";
import { PlayerEvents } from "./../interface/player-events.interface";
import {
  AdaptationSet,
  Period,
  MPDResponse,
  Representation
} from "../interface/mpd-response.interface";
import PlayerService from "../rest/PlayerService";
import { PlayerState } from "../interface/player.interface";

export class Player implements PlayerInterface {
  events: PlayerEvents;

  mpdResponse: any;

  video: Video = {};

  state: PlayerState = {
    playing: false,
    paused: false,
    loaded: false,
    ready: false
  };

  isInit: boolean = false;

  constructor(private playerService: PlayerService) {
    this.state.playing = false;
    this.playerService.fetchManifest().then(response => {
      this.mpdResponse = response;
      this.init(response);
      this.isInit = true;
    });
  }

  init(mpdRes: MPDResponse) {
    this.video.videoAdaptation = this.getVideoAdaptation(mpdRes);
    this.video.audioAdaptation = this.getAudioAdaptation(mpdRes);

    // loads video source
    this.load();
    this.video.audioSource = this.loadAudioSource();
    this.video.playbackDuration = this.getDuration(this.video.videoAdaptation);

    this.video.chosenRepresentation = this.getBestRepresentation(this.video);

    const videoCodec = this.video.videoAdaptation.$.codecs;
    const audioCodec = this.video.audioAdaptation.$.codecs;
    this.video.codecs = [];
    this.video.codecs.push(videoCodec, audioCodec);
  }

  getBestRepresentation(video: Video): any {
    /**
     * note: this should be calculated by the clients bandwidth.
     * Assuming here that 1080p is the best representation.
     */
    let r: Representation;
    video.videoAdaptation.Representation.forEach(rep => {
      if (rep.$.height === "1080") {
        r = rep;
      }
    });
    return r;
  }

  getVideoAdaptation(mpdRes: MPDResponse): AdaptationSet {
    let adaptation: AdaptationSet;
    mpdRes.Period[0].AdaptationSet.forEach(set => {
      if (set.$.mimeType.includes("video")) {
        adaptation = set;
      }
    });
    return adaptation;
  }

  getAudioAdaptation(mpdRes: MPDResponse): AdaptationSet {
    let adaptation: AdaptationSet;
    mpdRes.Period[0].AdaptationSet.forEach(set => {
      if (set.$.mimeType.includes("audio")) {
        adaptation = set;
      }
    });
    return adaptation;
  }

  getDuration(videoAdaptation: AdaptationSet): number {
    let duration = parseInt(videoAdaptation.SegmentTemplate[0].$.duration);
    let timescale = parseInt(videoAdaptation.SegmentTemplate[0].$.timescale);
    const minutes = Math.floor(duration / timescale);
    return Math.floor(minutes * 60);
  }

  loadAudioSource(): string {
    let mediaSource;
    this.mpdResponse.Period[0].AdaptationSet.forEach(set => {
      if (set.$.mimeType.includes("audio")) {
        mediaSource = set.SegmentTemplate[0].$.media;
      }
    });
    return mediaSource;
  }

  // break into rest api & stub responses
  fetch() {}
  stream() {}

  play() {
    this.state.playing = true;
  }

  pause() {
    if (this.state.playing) {
      this.state.playing = false;
      this.state.paused = true;
      return;
    }
  }

  stop() {
    this.state.playing = false;
  }

  load() {
    this.mpdResponse.Period[0].AdaptationSet.forEach(set => {
      if (set.$.mimeType.includes("video")) {
        this.video.videoSource = set.SegmentTemplate[0].$.media;
      }
    });
  }

  unload() {
    this.video = {};
  }

  // register and unregister event listeners.
  on() {}
  off() {}

  isPlaying(): boolean {
    return this.state.playing;
  }

  isPaused(): boolean {
    return this.state.paused;
  }

  isReady(): boolean {
    if (this.isInit) {
      return true;
    }
    return false;
  }

  isLoaded(): boolean {
    return this.video.videoSource !== null;
  }

  // used by player controls or by player to improve video stream
  changeRepresentation(type: string) {}

  destroy() {
    this.video = {};
  }
  getCurrentTime(): number {
    return this.video.playbackDuration;
  }
}

export default Player;
