import { MPDResponse } from "../interface/mpd-response.interface";
import axios from "../../node_modules/axios";

export class PlayerService {
  fetchManifest(): Promise<MPDResponse> {
    let mpdRes: MPDResponse;
    var parseString = require("xml2js").parseString;

    return new Promise(function(resolve, reject) {
      axios
        .get(
          "https://bitmovin-a.akamaihd.net/content/MI201109210084_1/mpds/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.mpd"
        )
        .then(function(response) {
          var xml = response.data;
          parseString(xml, function(err, result) {
            mpdRes = result.MPD;
          });
          resolve(mpdRes);
        })
        .catch(function(error) {
          console.log(error);
        });
    });
  }
}

export default PlayerService;
