"use strict";
exports.__esModule = true;
var axios_1 = require("../../node_modules/axios");
var PlayerService = /** @class */ (function () {
    function PlayerService() {
    }
    PlayerService.prototype.fetchManifest = function () {
        var mpdRes;
        var parseString = require("xml2js").parseString;
        return new Promise(function (resolve, reject) {
            axios_1["default"]
                .get("https://bitmovin-a.akamaihd.net/content/MI201109210084_1/mpds/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.mpd")
                .then(function (response) {
                var xml = response.data;
                parseString(xml, function (err, result) {
                    mpdRes = result.MPD;
                });
                resolve(mpdRes);
            })["catch"](function (error) {
                console.log(error);
            });
        });
    };
    return PlayerService;
}());
exports.PlayerService = PlayerService;
exports["default"] = PlayerService;
