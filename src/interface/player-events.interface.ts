export interface PlayerEvents {
  // fired when the player has loaded one video and audio segment
  ready: {
    listeners: any[];
  };

  // fired when playback time(duration) of the player has changed.
  timeChanged: [];

  // fired parses and loads manifest, player is initialised
  sourceLoaded: [];

  // fired from a video which is removed from a player
  sourceUnloaded: [];

  // fired when the player stars or resumes playback
  play: [];

  // fired when the player is paused
  pause: [];

  // fired when playback has finished
  playbackFinished: [];

  // fired when an error occurs and the player cannot recover
  error: [];
}
