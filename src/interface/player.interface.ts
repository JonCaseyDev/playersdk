import { Representation, AdaptationSet } from "./mpd-response.interface";

export interface PlayerInterface {
  fetch();

  stream();

  play();

  pause();

  stop();

  getDuration(videoAdaptation: AdaptationSet): number;

  load();

  unload();

  on();

  off();

  isPlaying(): boolean;

  isPaused(): boolean;

  isReady(): boolean;

  isLoaded(): boolean;

  changeRepresentation(type: string);

  destroy();

  getCurrentTime();
}

export interface PlayerState {
  ready: boolean;
  playing: boolean;
  paused: boolean;
  loaded: boolean;
}

export interface Video {
  videoSource?: string;
  audioSource?: string;
  chosenRepresentation?: Representation;
  playbackDuration?: number;
  codecs?: any[];
  videoAdaptation?: AdaptationSet;
  audioAdaptation?: AdaptationSet;
}
