export interface MPDResponse {
  id: string;
  profiles: string;
  type: string;
  availabilityStartTime: string;
  publishTime: string;
  mediaPresentationDuration: string;
  minBufferTime: string;
  bitMovinVersion: string;
  xmlnsns2: string;
  xmlns: string;
  xmlnsbitmovin: string;

  Period: Period;
}

export interface Period {}

export interface AdaptationSet {
  $: {
    mimeType: string;
    codecs: string;
  };

  AudioChannelConfiguration?: AudioChannelConfiguration[];
  SegmentTemplate: SegmentTemplate[];
  Representation: Representation[];
}

export interface AudioChannelConfiguration {
  $: {
    schemeIdUri: string;
    value: string;
  };
}

export interface SegmentTemplate {
  $: {
    media: string;
    initialization: string;
    duration: string;
    startNumber: string;
    timescale: string;
  };
}

export interface Representation {
  $: {
    id: string;
    bandwidth: string;
    width: string;
    height: string;
    frameRate: string;
  };
}
